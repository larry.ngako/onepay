package fr.decathlon.onepay.application.dto;

import java.math.BigDecimal;
import java.util.UUID;

public class UpdateItemDto {
    private UUID itemDtoId;
    private String name;
    private BigDecimal price;
    private Integer quantity;

    private UUID transactionDtoId;

    public UpdateItemDto() {
    }

    public UpdateItemDto(UUID itemDtoId, String name, BigDecimal price, Integer quantity, UUID transactionDtoId) {
        this.itemDtoId = itemDtoId;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.transactionDtoId = transactionDtoId;
    }

    public UpdateItemDto(String name, BigDecimal price, Integer quantity, UUID transactionDtoId) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.transactionDtoId = transactionDtoId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public UUID getItemDtoId() {
        return itemDtoId;
    }

    public void setItemDtoId(UUID itemDtoId) {
        this.itemDtoId = itemDtoId;
    }

    public UUID getTransactionDtoId() {
        return transactionDtoId;
    }

    public void setTransactionDtoId(UUID transactionDtoId) {
        this.transactionDtoId = transactionDtoId;
    }
}
