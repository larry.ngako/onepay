package fr.decathlon.onepay.application.dto;

import fr.decathlon.onepay.domain.util.PaymentType;

import java.io.Serializable;
import java.util.Set;

public class CreateTransactionDto implements Serializable {
    private PaymentType paymentType;
    private Set<ItemDto> itemDtos;

    public CreateTransactionDto() {
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Set<ItemDto> getItemDtos() {
        return itemDtos;
    }

    public void setItemDtos(Set<ItemDto> itemDtos) {
        this.itemDtos = itemDtos;
    }
}
