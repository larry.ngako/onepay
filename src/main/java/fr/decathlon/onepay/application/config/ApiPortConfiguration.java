package fr.decathlon.onepay.application.config;

import fr.decathlon.onepay.domain.api.ApiTransactionPort;
import fr.decathlon.onepay.domain.service.LogicService;
import fr.decathlon.onepay.domain.spy.TransactionPersistancePort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiPortConfiguration {

    @Bean
    ApiTransactionPort transactionPort(TransactionPersistancePort transactionPersistancePort) {
        return new LogicService(transactionPersistancePort);
    }

}
