package fr.decathlon.onepay.application.exception;

import fr.decathlon.onepay.domain.exception.BadRequestException;
import fr.decathlon.onepay.domain.exception.ForbiddenException;
import fr.decathlon.onepay.domain.exception.InternalServerError;
import fr.decathlon.onepay.domain.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final String MESSAGE = "message";
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    private static final String ERROR_LOGGING_MESSAGE_FORMAT = "Exception caught in handler : {}.";

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<Object> handleDomainException(BadRequestException ex) {
        logger.error(ERROR_LOGGING_MESSAGE_FORMAT, ex.getMessage(), ex);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put(MESSAGE, ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(body);

    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<Object> handleDomainException(ForbiddenException ex) {
        logger.error(ERROR_LOGGING_MESSAGE_FORMAT, ex.getMessage(), ex);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put(MESSAGE, ex.getMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(body);

    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> handleDomainException(NotFoundException ex) {
        logger.error(ERROR_LOGGING_MESSAGE_FORMAT, ex.getMessage(), ex);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put(MESSAGE, ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(body);

    }

    @ExceptionHandler(InternalServerError.class)
    public ResponseEntity<Object> handleDomainException(InternalServerError ex) {
        logger.error(ERROR_LOGGING_MESSAGE_FORMAT, ex.getMessage(), ex);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put(MESSAGE, ex.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception ex) {
        logger.error(ERROR_LOGGING_MESSAGE_FORMAT, ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Une erreur s'est produite.");
    }
}
