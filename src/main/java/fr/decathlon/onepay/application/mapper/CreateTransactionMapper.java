package fr.decathlon.onepay.application.mapper;

import fr.decathlon.onepay.application.dto.CreateTransactionDto;
import fr.decathlon.onepay.domain.model.ItemModel;
import fr.decathlon.onepay.domain.model.TransactionModel;

import java.util.Set;
import java.util.stream.Collectors;

public interface CreateTransactionMapper {

    static TransactionModel createTransactionDtoToTransactionModel(CreateTransactionDto transactionDto) {
        if (transactionDto == null) {
            return null;
        }
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setPaymentType(transactionDto.getPaymentType());
        Set<ItemModel> itemModels = transactionDto.getItemDtos().stream().map(CreateItemMapper::itemDtoToItemModel).collect(Collectors.toSet());
        transactionModel.setItemModels(itemModels);
        return transactionModel;

    }
}
