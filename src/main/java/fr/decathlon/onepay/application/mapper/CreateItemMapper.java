package fr.decathlon.onepay.application.mapper;

import fr.decathlon.onepay.application.dto.ItemDto;
import fr.decathlon.onepay.domain.model.ItemModel;

public interface CreateItemMapper {

    static ItemModel itemDtoToItemModel(ItemDto itemDto){
        if(itemDto == null) {
            return null;
        }
        ItemModel itemModel = new ItemModel();
        itemModel.setName(itemDto.getName());
        itemModel.setPrice(itemDto.getPrice());
        itemModel.setQuantity(itemDto.getQuantity());
        return itemModel;
    }
}
