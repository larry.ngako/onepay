package fr.decathlon.onepay.application.mapper;


import fr.decathlon.onepay.application.dto.UpdateItemDto;
import fr.decathlon.onepay.domain.model.ItemModel;

public interface UpdateItemMapper {

    static ItemModel itemDtoToItemModel(UpdateItemDto updateItemDto){
        if(updateItemDto == null) {
            return null;
        }
        ItemModel itemModel = new ItemModel();
        itemModel.setItemDtoId(updateItemDto.getItemDtoId());
        itemModel.setName(updateItemDto.getName());
        itemModel.setPrice(updateItemDto.getPrice());
        itemModel.setQuantity(updateItemDto.getQuantity());
        itemModel.setTransactionId(updateItemDto.getTransactionDtoId());
        return itemModel;
    }
}
