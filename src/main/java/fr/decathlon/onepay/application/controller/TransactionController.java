package fr.decathlon.onepay.application.controller;

import fr.decathlon.onepay.application.dto.CreateTransactionDto;
import fr.decathlon.onepay.application.dto.UpdateItemDto;
import fr.decathlon.onepay.application.mapper.CreateTransactionMapper;
import fr.decathlon.onepay.application.mapper.UpdateItemMapper;
import fr.decathlon.onepay.domain.api.ApiTransactionPort;
import fr.decathlon.onepay.domain.model.ItemModel;
import fr.decathlon.onepay.domain.model.TransactionModel;
import fr.decathlon.onepay.domain.util.PaymentStatus;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/transactions")
public class TransactionController {

    private final ApiTransactionPort apiTransactionPort;

    public TransactionController(ApiTransactionPort apiTransactionPort) {
        this.apiTransactionPort = apiTransactionPort;
    }

    @PostMapping
    public ResponseEntity<TransactionModel> createTransaction(@RequestBody CreateTransactionDto createTransactionDto) {
        return new ResponseEntity<>(apiTransactionPort.createTransaction(
                CreateTransactionMapper.createTransactionDtoToTransactionModel(createTransactionDto)), HttpStatus.CREATED);
    }

    @PutMapping("/{id}/status")
    public ResponseEntity<TransactionModel> updateTransactionStatus(@PathVariable UUID id, @RequestParam PaymentStatus status) {
        return new ResponseEntity<>(apiTransactionPort.updateTransactionStatus(id, status), HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<Set<ItemModel>> addOrUpdateItemsOfTransaction(@RequestBody Set<UpdateItemDto> updateItemDtos) {
        var itemsModel=  updateItemDtos.stream().map(UpdateItemMapper::itemDtoToItemModel).collect(Collectors.toSet());
        return new ResponseEntity<>(apiTransactionPort.addOrUpdateItemsOfTransaction(itemsModel), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Set<ItemModel>> deleteItemsOfTransaction(@PathVariable UUID id, @RequestBody Set<UUID> itemsId) {
         return new ResponseEntity<>(apiTransactionPort.deleteItemsOfTransaction(id, itemsId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Set<TransactionModel>> getAllTransactions() {
        return new ResponseEntity<>(apiTransactionPort.getAllTransactions(), HttpStatus.OK);
    }
}
