package fr.decathlon.onepay.infrastructure.mapper;

import fr.decathlon.onepay.domain.model.ItemModel;
import fr.decathlon.onepay.infrastructure.entity.Item;

public interface ItemMapper {

     static ItemModel itemToItemDto(Item item) {
        if (item == null) {
            return null;
        }
        ItemModel itemModel = new ItemModel(item.getItemId(), item.getName(), item.getPrice(),
                item.getQuantity(), item.getTransaction().getTransactionId());

        return itemModel;
    }
    static Item itemDtoToItem (ItemModel itemModel) {
        if (itemModel == null) {
            return null;
        }

        Item item = new Item();
        item.setItemId(itemModel.getItemDtoId());
        item.setName(itemModel.getName());
        item.setPrice(itemModel.getPrice());
        item.setQuantity(itemModel.getQuantity());

        return item;
    }

}
