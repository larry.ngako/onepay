package fr.decathlon.onepay.infrastructure.mapper;

import fr.decathlon.onepay.domain.model.TransactionModel;
import fr.decathlon.onepay.infrastructure.entity.Item;
import fr.decathlon.onepay.infrastructure.entity.Transaction;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public interface TransactionMapper {

    static Transaction transactionDtoToTransaction (TransactionModel transactionModel) {
        if (transactionModel == null) {
            return null;
        }

        Transaction transaction = new Transaction();
        transaction.setTransactionId(transactionModel.getTransactionId());
        transaction.setPaymentStatus(transactionModel.getPaymentStatus());
        transaction.setPaymentType(transactionModel.getPaymentType());
        transaction.setTotalAmount(transactionModel.getTotalAmount());
        Set<Item> items = new HashSet<>(transactionModel.getItemModels().stream()
                .map(ItemMapper::itemDtoToItem)
                .toList());
        transaction.setItems(items);

        return transaction;
    }

    static TransactionModel transactionToTransactionDto (Transaction transaction) {
        if (transaction == null) {
            return null;
        }

        TransactionModel transactionModel = new TransactionModel(transaction.getTransactionId(), transaction.getTotalAmount(),
                transaction.getPaymentType(),
                transaction.getItems().stream()
                        .map(ItemMapper::itemToItemDto)
                        .collect(Collectors.toSet()),
                transaction.getPaymentStatus());


        return transactionModel;
    }

}
