package fr.decathlon.onepay.infrastructure.adapter;

import fr.decathlon.onepay.domain.model.ItemModel;
import fr.decathlon.onepay.domain.model.TransactionModel;
import fr.decathlon.onepay.domain.exception.NotFoundException;
import fr.decathlon.onepay.domain.spy.TransactionPersistancePort;
import fr.decathlon.onepay.infrastructure.entity.Item;
import fr.decathlon.onepay.infrastructure.mapper.ItemMapper;
import fr.decathlon.onepay.infrastructure.mapper.TransactionMapper;
import fr.decathlon.onepay.infrastructure.repository.ItemRepository;
import fr.decathlon.onepay.infrastructure.repository.TransactionRepository;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class TransactionAdapter implements TransactionPersistancePort {
    private final TransactionRepository transactionRepository;

    private final ItemRepository itemRepository;

    public TransactionAdapter(TransactionRepository transactionRepository, ItemRepository itemRepository) {
        this.transactionRepository = transactionRepository;
        this.itemRepository = itemRepository;
    }

    @Override
    public TransactionModel createOrUpdateTransaction(TransactionModel transactionModel) {


        var transaction = TransactionMapper.transactionDtoToTransaction(transactionModel);
        transaction.getItems().forEach(item -> item.setTransaction(transaction));
        transactionRepository.save(transaction);
        return TransactionMapper.transactionToTransactionDto(
                transaction);
    }


    @Override
    public TransactionModel findTransactionById (UUID id) {
        var transaction = transactionRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Transaction not found"));
        return TransactionMapper.transactionToTransactionDto(transaction);
    }

    @Override
    public Set<ItemModel> addOrUpdateItemsOfTransaction(Set<ItemModel> itemModels) {
        var transaction = transactionRepository.findById(itemModels.stream().findFirst().get().getTransactionId())
                .orElseThrow(() -> new NotFoundException("Transaction not found"));
        var items = itemModels.stream().map(ItemMapper::itemDtoToItem).collect(Collectors.toSet());
        items.stream().forEach(item -> {
            item.setTransaction(transaction);
        });
        itemRepository.saveAll(items);

        var itemsOfTransactions = itemRepository.findByTransaction(transaction);
        transaction.setTotalAmount(calculateTotalAmount(itemsOfTransactions));
        transactionRepository.save(transaction);
       return items.stream()
               .map(ItemMapper::itemToItemDto)
               .collect(Collectors.toSet());
    }

    private BigDecimal calculateTotalAmount(Set<Item> item) {
        return item.stream()
                .map(i -> i.getPrice().multiply(BigDecimal.valueOf(i.getQuantity())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public Set<ItemModel> deleteItemsOfTransaction(UUID transactionId, Set<UUID> itemModels) {
        var items = itemRepository.findAllById(itemModels);
        itemRepository.deleteAll(items);
       var transaction = transactionRepository.findById(transactionId)
                .orElseThrow(() -> new NotFoundException("Transaction not found"));
       transaction.getItems().removeAll(items);
       var itemsOfTransactions = itemRepository.findByTransaction(transaction);
       transaction.setTotalAmount(calculateTotalAmount(itemsOfTransactions)
               .subtract(calculateTotalAmount(items.stream().collect(Collectors.toSet()))));
       transactionRepository.save(transaction);
       return transaction.getItems().stream()
                .map(ItemMapper::itemToItemDto)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<TransactionModel> getAllTransactions() {
        var transactions = transactionRepository.findAll();
        return transactions.stream()
                .map(TransactionMapper::transactionToTransactionDto)
                .collect(Collectors.toSet());
    }
}
