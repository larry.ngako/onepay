package fr.decathlon.onepay.infrastructure.repository;

import fr.decathlon.onepay.infrastructure.entity.Item;
import fr.decathlon.onepay.infrastructure.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;
@Repository
public interface ItemRepository extends JpaRepository<Item, UUID> {

    Set<Item> findByTransaction(Transaction transaction);
}
