package fr.decathlon.onepay.infrastructure.repository;

import fr.decathlon.onepay.infrastructure.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, UUID> {
}
