package fr.decathlon.onepay.infrastructure.config;

import fr.decathlon.onepay.domain.spy.TransactionPersistancePort;
import fr.decathlon.onepay.infrastructure.adapter.TransactionAdapter;
import fr.decathlon.onepay.infrastructure.repository.ItemRepository;
import fr.decathlon.onepay.infrastructure.repository.TransactionRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpyPortConfiguration {

    @Bean
    TransactionPersistancePort transactionPersistancePort(TransactionRepository transactionRepository,
                                                          ItemRepository itemRepository) {
        return new TransactionAdapter(transactionRepository, itemRepository);
    }
}
