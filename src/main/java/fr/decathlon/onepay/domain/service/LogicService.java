package fr.decathlon.onepay.domain.service;

import fr.decathlon.onepay.domain.api.ApiTransactionPort;
import fr.decathlon.onepay.domain.model.ItemModel;
import fr.decathlon.onepay.domain.model.TransactionModel;
import fr.decathlon.onepay.domain.exception.BadRequestException;
import fr.decathlon.onepay.domain.spy.TransactionPersistancePort;
import fr.decathlon.onepay.domain.util.PaymentStatus;

import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

public class LogicService implements ApiTransactionPort {
    private final TransactionPersistancePort transactionPersistancePort;

    public LogicService(TransactionPersistancePort transactionPersistancePort) {
        this.transactionPersistancePort = transactionPersistancePort;
    }

    @Override
    public TransactionModel createTransaction(TransactionModel transactionModel) {
        transactionModel.setPaymentStatus(PaymentStatus.NEW);
        transactionModel.setTotalAmount(calculateTotalAmount(transactionModel.getItemModels()));

        return transactionPersistancePort.createOrUpdateTransaction(transactionModel);
    }

    private BigDecimal calculateTotalAmount(Set<ItemModel> itemModels) {
        return itemModels.stream()
                .map(itemModel -> itemModel.getPrice().multiply(BigDecimal.valueOf(itemModel.getQuantity())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public TransactionModel updateTransactionStatus(UUID id, PaymentStatus status) {
        var transactionModel = transactionPersistancePort.findTransactionById(id);
        if (transactionModel.getPaymentStatus() == PaymentStatus.CAPTURED
                || transactionModel.getPaymentStatus() == PaymentStatus.CANCELED) {
            throw new BadRequestException("Cannot modify a CAPTURED or CANCELED transaction");
        }

        if (status == PaymentStatus.CAPTURED &&
                transactionModel.getPaymentStatus() != PaymentStatus.AUTHORIZED) {
            throw new BadRequestException("Can only change status to CAPTURED from AUTHORIZED");
        }
        transactionModel.setPaymentStatus(status);
        return transactionPersistancePort.createOrUpdateTransaction(transactionModel);
    }

    @Override
    public Set<ItemModel> addOrUpdateItemsOfTransaction(Set<ItemModel> itemModels) {
        var transactionModel = transactionPersistancePort.findTransactionById(itemModels.stream()
                .findFirst().get().getTransactionId());
        if(transactionModel.getPaymentStatus() != PaymentStatus.NEW) {
            throw new BadRequestException("the purchase order can not be change");
        }
        return transactionPersistancePort.addOrUpdateItemsOfTransaction(itemModels);
    }

    @Override
    public Set<ItemModel> deleteItemsOfTransaction(UUID transactionId, Set<UUID> itemModelsId) {
        var transactionDto = transactionPersistancePort.findTransactionById(transactionId);
        if(transactionDto.getPaymentStatus() != PaymentStatus.NEW) {
            throw new BadRequestException("the purchase order can not be change");
        }
        return transactionPersistancePort.deleteItemsOfTransaction(transactionId, itemModelsId);
    }

    @Override
    public Set<TransactionModel> getAllTransactions() {
        return transactionPersistancePort.getAllTransactions();
    }


}
