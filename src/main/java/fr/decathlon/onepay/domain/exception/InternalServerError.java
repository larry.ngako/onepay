package fr.decathlon.onepay.domain.exception;

public class InternalServerError extends DomainException {

    public InternalServerError(String message) {
        super(message);
    }
}
