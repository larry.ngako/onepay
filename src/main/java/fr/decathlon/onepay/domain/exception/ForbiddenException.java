package fr.decathlon.onepay.domain.exception;

public class ForbiddenException extends DomainException {

    public ForbiddenException(String message) {
        super(message);
    }
}
