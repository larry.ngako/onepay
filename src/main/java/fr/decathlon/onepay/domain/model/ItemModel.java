package fr.decathlon.onepay.domain.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

public class ItemModel implements Serializable {

    private UUID itemDtoId;
    private String name;
    private BigDecimal price;
    private Integer quantity;
    private UUID transactionId;

    public ItemModel() {
    }

    public ItemModel(UUID itemDtoId, String name, BigDecimal price, Integer quantity, UUID transactionId) {
        this.itemDtoId = itemDtoId;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.transactionId = transactionId;
    }
    public ItemModel(String name, BigDecimal price, Integer quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public UUID getItemDtoId() {
        return itemDtoId;
    }

    public void setItemDtoId(UUID itemDtoId) {
        this.itemDtoId = itemDtoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public UUID getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(UUID transactionId) {
        this.transactionId = transactionId;
    }
}
