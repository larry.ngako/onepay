package fr.decathlon.onepay.domain.model;


import fr.decathlon.onepay.domain.util.PaymentStatus;
import fr.decathlon.onepay.domain.util.PaymentType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class TransactionModel implements Serializable {
    private UUID transactionId;
    private BigDecimal totalAmount;
    private PaymentType paymentType;
    private Set<ItemModel> itemModels;
    private PaymentStatus paymentStatus;

    public TransactionModel(UUID transactionId, BigDecimal totalAmount, PaymentType paymentType, Set<ItemModel> itemModels, PaymentStatus paymentStatus) {
        this.transactionId = transactionId;
        this.totalAmount = totalAmount;
        this.paymentType = paymentType;
        this.itemModels = itemModels;
        this.paymentStatus = paymentStatus;
    }

    public TransactionModel() {

    }

    public UUID getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(UUID transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Set<ItemModel> getItemModels() {
        return itemModels;
    }

    public void setItemModels(Set<ItemModel> itemModels) {
        this.itemModels = itemModels;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
