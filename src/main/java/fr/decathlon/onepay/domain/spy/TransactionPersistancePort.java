package fr.decathlon.onepay.domain.spy;

import fr.decathlon.onepay.domain.model.ItemModel;
import fr.decathlon.onepay.domain.model.TransactionModel;

import java.util.Set;
import java.util.UUID;

public interface TransactionPersistancePort {

    TransactionModel findTransactionById (UUID id);
    TransactionModel createOrUpdateTransaction (TransactionModel transactionModel);
    Set<TransactionModel> getAllTransactions();
    Set<ItemModel> addOrUpdateItemsOfTransaction(Set<ItemModel> itemModels);
    Set<ItemModel> deleteItemsOfTransaction(UUID transactionId, Set<UUID> itemModelsId);
}
