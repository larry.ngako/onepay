package fr.decathlon.onepay.domain.util;

public enum PaymentStatus {
    NEW, AUTHORIZED, CAPTURED, CANCELED
}
