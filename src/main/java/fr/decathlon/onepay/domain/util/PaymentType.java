package fr.decathlon.onepay.domain.util;

public enum PaymentType {
    CREDIT_CARD, GIFT_CARD, PAYPAL
}
