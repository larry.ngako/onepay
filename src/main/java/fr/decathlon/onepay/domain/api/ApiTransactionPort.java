package fr.decathlon.onepay.domain.api;

import fr.decathlon.onepay.domain.model.ItemModel;
import fr.decathlon.onepay.domain.model.TransactionModel;
import fr.decathlon.onepay.domain.util.PaymentStatus;

import java.util.Set;
import java.util.UUID;

public interface ApiTransactionPort {
    TransactionModel createTransaction(TransactionModel transactionModel);
    TransactionModel updateTransactionStatus(UUID id, PaymentStatus status);
    Set<ItemModel> addOrUpdateItemsOfTransaction(Set<ItemModel> itemModels);
    Set<ItemModel> deleteItemsOfTransaction(UUID transactionId, Set<UUID> itemModelsId);
    Set<TransactionModel> getAllTransactions();
}
