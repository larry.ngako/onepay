package fr.decathlon.onepay.application.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.decathlon.onepay.application.dto.CreateTransactionDto;
import fr.decathlon.onepay.application.dto.ItemDto;
import fr.decathlon.onepay.application.dto.UpdateItemDto;
import fr.decathlon.onepay.domain.util.PaymentStatus;
import fr.decathlon.onepay.domain.util.PaymentType;
import fr.decathlon.onepay.infrastructure.entity.Item;
import fr.decathlon.onepay.infrastructure.repository.ItemRepository;
import fr.decathlon.onepay.infrastructure.repository.TransactionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class TransactionControllerITest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        transactionRepository.deleteAll();
        itemRepository.deleteAll();
    }


    @Test
    void testCreateAndModifyTransaction() throws Exception {

        CreateTransactionDto transactionDto = new CreateTransactionDto();
        transactionDto.setPaymentType(PaymentType.CREDIT_CARD);
        transactionDto.setItemDtos(Set.of(new ItemDto("T-shirt", BigDecimal.valueOf(19.99), 5)));

        mockMvc.perform(post("/api/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transactionDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.NEW.name()));
        UUID transactionId = transactionRepository.findAll().get(0).getTransactionId();
        mockMvc.perform(put("/api/transactions/" + transactionId + "/status")
                        .param("status", PaymentStatus.AUTHORIZED.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.AUTHORIZED.name()));

        mockMvc.perform(put("/api/transactions/" + transactionId + "/status")
                        .param("status", PaymentStatus.CAPTURED.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.CAPTURED.name()));
    }

    @Test
    public void testCreateAndCancelTransaction() throws Exception {
        // Create a transaction
        CreateTransactionDto transactionDto = new CreateTransactionDto();
        transactionDto.setPaymentType(PaymentType.PAYPAL);
        transactionDto.setItemDtos(Set.of(
                new ItemDto("Bike", BigDecimal.valueOf(208.00), 1),
                new ItemDto("Shoes", BigDecimal.valueOf(30.00), 1)
        ));

        mockMvc.perform(post("/api/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transactionDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.NEW.name()));

        // Cancel the transaction
        UUID transactionId = transactionRepository.findAll().get(0).getTransactionId();

        mockMvc.perform(put("/api/transactions/" + transactionId + "/status")
                        .param("status", PaymentStatus.CANCELED.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.CANCELED.name()));
    }

    @Test
    public void testGetAllTransactions() throws Exception {

        CreateTransactionDto transactionDto1 = new CreateTransactionDto();
        transactionDto1.setPaymentType(PaymentType.CREDIT_CARD);
        transactionDto1.setItemDtos(Set.of(new ItemDto("T-shirt", BigDecimal.valueOf(19.99), 5)));

        mockMvc.perform(post("/api/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transactionDto1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.NEW.name()));

        CreateTransactionDto transactionDto2 = new CreateTransactionDto();
        transactionDto2.setPaymentType(PaymentType.PAYPAL);
        transactionDto2.setItemDtos(Set.of(
                new ItemDto("Bike", BigDecimal.valueOf(208.00), 1),
                new ItemDto("Shoes", BigDecimal.valueOf(30.00), 1)
        ));

        mockMvc.perform(post("/api/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transactionDto2)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.NEW.name()));


        mockMvc.perform(get("/api/transactions"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    void testModifyItemOfTransaction() throws Exception {

        CreateTransactionDto transactionDto = new CreateTransactionDto();
        transactionDto.setPaymentType(PaymentType.CREDIT_CARD);
        transactionDto.setItemDtos(Set.of(new ItemDto("T-shirt", BigDecimal.valueOf(19.99), 5)));

        mockMvc.perform(post("/api/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transactionDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.NEW.name()));
        var transaction = transactionRepository.findAll().get(0);
        UUID itemDtoId = transaction.getItems().stream()
                .filter(item -> item.getName().equalsIgnoreCase("T-shirt")
                ).map(Item::getItemId).findFirst().get();
        var itemDtos = Set.of(new UpdateItemDto(itemDtoId, "T-shirt", BigDecimal.valueOf(19.99), 4, transaction.getTransactionId()));
        mockMvc.perform(post("/api/transactions/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(itemDtos)));
        Assertions.assertEquals(transactionRepository.findById(transaction.getTransactionId()).get()
                .getTotalAmount(), BigDecimal.valueOf(79.96));
    }


    @Test
    void testAddItemOfTransaction() throws Exception {

        CreateTransactionDto transactionDto = new CreateTransactionDto();
        transactionDto.setPaymentType(PaymentType.CREDIT_CARD);
        transactionDto.setItemDtos(Set.of(new ItemDto("Bike", BigDecimal.valueOf(208.00), 1)));

        mockMvc.perform(post("/api/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transactionDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.NEW.name()));
        var transaction = transactionRepository.findAll().get(0);
        var itemDtos = Set.of(new UpdateItemDto("Shoes", BigDecimal.valueOf(30.00), 1, transaction.getTransactionId()));
        mockMvc.perform(post("/api/transactions/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(itemDtos)));
        var amount = transactionRepository.findById(transaction.getTransactionId()).get().getTotalAmount();
        assertThat(amount).isEqualByComparingTo(BigDecimal.valueOf(238.00));
    }

    @Test
    void testDeleteItemOfTransaction() throws Exception {

        CreateTransactionDto transactionDto = new CreateTransactionDto();
        transactionDto.setPaymentType(PaymentType.CREDIT_CARD);
        transactionDto.setItemDtos(Set.of(new ItemDto("Bike", BigDecimal.valueOf(208.00), 1),
                new ItemDto("Shoes", BigDecimal.valueOf(30.00), 1)));

        mockMvc.perform(post("/api/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transactionDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.NEW.name()));
        var transaction = transactionRepository.findAll().get(0);
        Set itemDtosId = transaction.getItems().stream()
                .filter(item -> item.getName().equalsIgnoreCase("Bike")
                ).map(Item::getItemId).collect(Collectors.toSet());
        mockMvc.perform(delete("/api/transactions/delete/" + transaction.getTransactionId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(itemDtosId)));
        var amount = transactionRepository.findById(transaction.getTransactionId()).get()
                .getTotalAmount();
        assertThat(amount).isEqualByComparingTo(BigDecimal.valueOf(30.00));
    }

    @Test
    void testCanNotModifyOrder() throws Exception {

        CreateTransactionDto transactionDto = new CreateTransactionDto();
        transactionDto.setPaymentType(PaymentType.CREDIT_CARD);
        transactionDto.setItemDtos(Set.of(new ItemDto("T-shirt", BigDecimal.valueOf(19.99), 5)));

        mockMvc.perform(post("/api/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transactionDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.NEW.name()));

        UUID transactionId = transactionRepository.findAll().get(0).getTransactionId();
        mockMvc.perform(put("/api/transactions/" + transactionId + "/status")
                        .param("status", PaymentStatus.AUTHORIZED.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.paymentStatus").value(PaymentStatus.AUTHORIZED.name()));

        var transaction = transactionRepository.findAll().get(0);
        UUID itemDtoId = transaction.getItems().stream()
                .filter(item -> item.getName().equalsIgnoreCase("T-shirt")
                ).map(Item::getItemId).findFirst().get();
        var itemDtos = Set.of(new UpdateItemDto(itemDtoId, "T-shirt", BigDecimal.valueOf(19.99), 4, transaction.getTransactionId()));
        mockMvc.perform(post("/api/transactions/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(itemDtos)))
                .andExpect(jsonPath("$.message").value("the purchase order can not be change"));
    }
}
