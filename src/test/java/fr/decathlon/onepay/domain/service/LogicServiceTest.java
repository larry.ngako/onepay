package fr.decathlon.onepay.domain.service;

import fr.decathlon.onepay.domain.exception.BadRequestException;
import fr.decathlon.onepay.domain.model.ItemModel;
import fr.decathlon.onepay.domain.model.TransactionModel;
import fr.decathlon.onepay.domain.spy.TransactionPersistancePort;
import fr.decathlon.onepay.domain.util.PaymentStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LogicServiceTest {

    private TransactionPersistancePort transactionPersistancePort = mock(TransactionPersistancePort.class);

    private LogicService logicService;

    @BeforeEach
    void setUp() {
        logicService = new LogicService(transactionPersistancePort);
    }

    @Test
    void testCreateTransaction_NewPaymentStatus() {
        // given
        TransactionModel transactionModel = new TransactionModel();
        ItemModel itemModel = new ItemModel();
        itemModel.setPrice(BigDecimal.valueOf(100));
        itemModel.setQuantity(2);
        Set<ItemModel> items = new HashSet<>();
        items.add(itemModel);
        transactionModel.setItemModels(items);

        when(transactionPersistancePort.createOrUpdateTransaction(any(TransactionModel.class)))
                .thenReturn(transactionModel);

        // when
        TransactionModel result = logicService.createTransaction(transactionModel);

        // then
        assertNotNull(result);
        assertEquals(PaymentStatus.NEW, result.getPaymentStatus());
        assertEquals(BigDecimal.valueOf(200), result.getTotalAmount());
        verify(transactionPersistancePort, times(1)).createOrUpdateTransaction(transactionModel);
    }

    @Test
    void testCreateTransaction() {
        // given
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setItemModels(Set.of(new ItemModel("birk", BigDecimal.valueOf(33.50), 5)));
        BigDecimal expectedTotalAmount = BigDecimal.valueOf(167.5);

        when(transactionPersistancePort.createOrUpdateTransaction(transactionModel))
                .thenReturn(transactionModel);

        // when
        TransactionModel result = logicService.createTransaction(transactionModel);

        // then
        assertNotNull(result);
        assertEquals(PaymentStatus.NEW, result.getPaymentStatus());
        assertEquals(expectedTotalAmount, result.getTotalAmount());
        verify(transactionPersistancePort, times(1)).createOrUpdateTransaction(transactionModel);
    }

    @Test
    void testUpdateTransactionStatus_InvalidStatusChange() {
        // given
        UUID transactionId = UUID.randomUUID();
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setPaymentStatus(PaymentStatus.CAPTURED);

        when(transactionPersistancePort.findTransactionById(transactionId))
                .thenReturn(transactionModel);

        // when / then
        BadRequestException exception = assertThrows(BadRequestException.class, () -> {
            logicService.updateTransactionStatus(transactionId, PaymentStatus.AUTHORIZED);
        });

        assertEquals("Cannot modify a CAPTURED or CANCELED transaction", exception.getMessage());
        verify(transactionPersistancePort, never()).createOrUpdateTransaction(any(TransactionModel.class));
    }

    @Test
    void testAddOrUpdateItemsOfTransaction_InvalidStatus() {
        // given
        ItemModel itemModel = new ItemModel();
        itemModel.setTransactionId(UUID.randomUUID());
        Set<ItemModel> items = new HashSet<>();
        items.add(itemModel);
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setPaymentStatus(PaymentStatus.CAPTURED);

        when(transactionPersistancePort.findTransactionById(itemModel.getTransactionId()))
                .thenReturn(transactionModel);

        // when / then
        BadRequestException exception = assertThrows(BadRequestException.class, () -> {
            logicService.addOrUpdateItemsOfTransaction(items);
        });

        assertEquals("the purchase order can not be change", exception.getMessage());
        verify(transactionPersistancePort, never()).addOrUpdateItemsOfTransaction(anySet());
    }

    @Test
    void testDeleteItemsOfTransaction_InvalidStatus() {
        // given
        UUID transactionId = UUID.randomUUID();
        UUID itemId = UUID.randomUUID();
        Set<UUID> itemIds = new HashSet<>();
        itemIds.add(itemId);
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setPaymentStatus(PaymentStatus.CAPTURED);

        when(transactionPersistancePort.findTransactionById(transactionId))
                .thenReturn(transactionModel);

        // when / then
        BadRequestException exception = assertThrows(BadRequestException.class, () -> {
            logicService.deleteItemsOfTransaction(transactionId, itemIds);
        });

        assertEquals("the purchase order can not be change", exception.getMessage());
        verify(transactionPersistancePort, never()).deleteItemsOfTransaction(any(UUID.class), anySet());
    }

    @Test
    void testGetAllTransactions() {
        // given
        Set<TransactionModel> transactions = new HashSet<>();
        TransactionModel transactionModel = new TransactionModel();
        transactions.add(transactionModel);

        when(transactionPersistancePort.getAllTransactions()).thenReturn(transactions);

        // when
        Set<TransactionModel> result = logicService.getAllTransactions();

        // then
        assertNotNull(result);
        assertEquals(1, result.size());
        verify(transactionPersistancePort, times(1)).getAllTransactions();
    }
}
