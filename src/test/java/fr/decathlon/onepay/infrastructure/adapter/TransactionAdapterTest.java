package fr.decathlon.onepay.infrastructure.adapter;

import fr.decathlon.onepay.domain.exception.NotFoundException;
import fr.decathlon.onepay.domain.model.ItemModel;
import fr.decathlon.onepay.domain.model.TransactionModel;
import fr.decathlon.onepay.infrastructure.entity.Item;
import fr.decathlon.onepay.infrastructure.entity.Transaction;
import fr.decathlon.onepay.infrastructure.repository.ItemRepository;
import fr.decathlon.onepay.infrastructure.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransactionAdapterTest {
     private final TransactionRepository transactionRepository = mock(TransactionRepository.class);

    private final ItemRepository itemRepository = mock(ItemRepository.class);

    private  TransactionAdapter transactionAdapter;

    @BeforeEach
    void setUp() {
        transactionAdapter = new TransactionAdapter(transactionRepository,itemRepository);
    }

    @Test
    void testCreateOrUpdateTransaction() {
        // given
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setItemModels(Set.of(new ItemModel("birk", BigDecimal.valueOf(33.50), 5)));
        Transaction transaction = new Transaction();
        transaction.setTransactionId(UUID.randomUUID());
        transaction.setItems(Set.of(new Item("birk", BigDecimal.valueOf(33.50), 5)));

        when(transactionRepository.save(transaction)).thenReturn(transaction);

        // when
        TransactionModel result = transactionAdapter.createOrUpdateTransaction(transactionModel);

        // then
        assertNotNull(result);
    }

    @Test
    void testFindTransactionById_TransactionDoesNotExist() {
        // given
        UUID transactionId = UUID.randomUUID();

        when(transactionRepository.findById(transactionId)).thenReturn(Optional.empty());

        // when / then
        assertThrows(NotFoundException.class, () -> transactionAdapter.findTransactionById(transactionId));
        verify(transactionRepository, times(1)).findById(transactionId);
    }

    @Test
    void testAddOrUpdateItemsOfTransaction() {
        // given
        UUID transactionId = UUID.randomUUID();
        ItemModel itemModel = new ItemModel("birk", BigDecimal.valueOf(33.50), 5);
        itemModel.setTransactionId(transactionId);
        Set<ItemModel> itemModels = new HashSet<>(Collections.singletonList(itemModel));
        Transaction transaction = new Transaction();
        transaction.setTransactionId(transactionId);
        Item item = new Item("birk", BigDecimal.valueOf(33.50), 5);
        item.setTransaction(transaction);
        Set<Item> items = new HashSet<>(Collections.singletonList(item));

        when(transactionRepository.findById(transactionId)).thenReturn(Optional.of(transaction));
       // when(itemRepository.saveAll(anySet())).thenReturn(items);
        when(itemRepository.findByTransaction(transaction)).thenReturn(items);

        // when
        Set<ItemModel> result = transactionAdapter.addOrUpdateItemsOfTransaction(itemModels);

        // then
        assertNotNull(result);
        assertEquals(1, result.size());
        verify(transactionRepository, times(1)).findById(transactionId);
        verify(itemRepository, times(1)).saveAll(anySet());
        verify(transactionRepository, times(1)).save(transaction);
    }

    @Test
    void testDeleteItemsOfTransaction() {
        // given
        UUID transactionId = UUID.randomUUID();
        UUID itemId = UUID.randomUUID();
        Set<UUID> itemModels = new HashSet<>(Collections.singletonList(itemId));
        Transaction transaction = new Transaction();
        transaction.setTransactionId(transactionId);
        Item item = new Item("birk", BigDecimal.valueOf(33.50), 5);
        item.setTransaction(transaction);
        Set<Item> items = new HashSet<>(Collections.singletonList(item));

       // when(itemRepository.findAllById(itemModels)).thenReturn(items);
        doNothing().when(itemRepository).deleteAll(items);
        when(transactionRepository.findById(transactionId)).thenReturn(Optional.of(transaction));
        when(itemRepository.findByTransaction(transaction)).thenReturn(new HashSet<>());

        // when
        Set<ItemModel> result = transactionAdapter.deleteItemsOfTransaction(transactionId, itemModels);

        // then
        assertNotNull(result);
        assertEquals(0, result.size());
        verify(itemRepository, times(1)).findAllById(itemModels);
        verify(transactionRepository, times(1)).findById(transactionId);
        verify(transactionRepository, times(1)).save(transaction);
    }

    @Test
    void testGetAllTransactions() {
        // given
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setTransactionId(UUID.randomUUID());
        transactionModel.setItemModels(Set.of(new ItemModel("birk", BigDecimal.valueOf(33.50), 5)));
        Transaction transaction = new Transaction();
        transaction.setTransactionId(UUID.randomUUID());
        Item item = new Item("birk", BigDecimal.valueOf(33.50), 5);
        item.setTransaction(transaction);
        transaction.setItems(Set.of(item));
        List<Transaction> transactions = Collections.singletonList(transaction);

        when(transactionRepository.findAll()).thenReturn(transactions);
        // when
        Set<TransactionModel> result = transactionAdapter.getAllTransactions();

        // then
        assertNotNull(result);
        assertEquals(1, result.size());
        verify(transactionRepository, times(1)).findAll();
    }
}
