# onepay



## Technical Test Java Backend - DECATHLON


## Description

Spring Boot application exposing a RESTful API to manage payments


## DATA base

H2 Database

## Documentaion:

Open API : http://localhost:8080/api-docs

Swagger UI : http://localhost:8080/swagger-ui/index.html

## Test
Integration test with SpringBootTest
Unit test with Junit


